# Spotter Android Client #

Here lies the spotter android client (mvp)

To run a develop build, first edit: `<string name="api_host">http://10.0.3.2:3000/api/</string>` in `spotter-android-client/app/src/debug/res/values/strings.xml` to point to the address of the spotter-server api instance... Then call: `./gradlew assembleDebug` and sideload the apk or select `Run > Run 'app'` in Android Studio.


To load fake marker data, edit these constants in the AppConstants.java class
`SPOTS_CALL_RADIUS, TEST_SPOTS_COUNT, ADD_MARKER_INTERVAL_DELAY, SHOULD_LOAD_FAKE_DATA`