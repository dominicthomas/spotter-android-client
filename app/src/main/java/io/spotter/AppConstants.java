package io.spotter;

public class AppConstants {

	public class Map {
		public static final String MAP_STATE_BUNDLE_KEY = "MAP_STATE_BUNDLE_KEY";
		public static final String PREFS_KEY_REQUEST_LAT = "PREFS_KEY_REQUEST_LAT";
		public static final String PREFS_KEY_REQUEST_LON = "PREFS_KEY_REQUEST_LON";
		public static final int INITIAL_ZOOM_LEVEL = 16;
		public static final int UPDATE_INTERVAL = 15000;
		public static final int FASTEST_UPDATE_INTERVAL = 5000;
		public static final int SPOTS_CALL_RADIUS = 5000;
		public static final int TEST_SPOTS_COUNT = 250;
		public static final int ADD_MARKER_INTERVAL_DELAY = 90;
		public static final boolean SHOULD_LOAD_FAKE_DATA = true;
		public static final float MIN_ZOOM_LEVEL_TO_LOAD_SPOTS = 10f;
		public static final float MIN_ZOOM_LEVEL_TO_RENDER_MARKER_THUMB = 15.0f;
		public static final int REQUEST_CHECK_LOCATION_SETTINGS = 1001;
	}

}
