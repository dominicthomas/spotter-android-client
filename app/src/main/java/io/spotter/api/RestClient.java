package io.spotter.api;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.Map;

import io.realm.RealmObject;
import io.spotter.BuildConfig;
import io.spotter.R;
import io.spotter.api.response.CategoryResponse;
import io.spotter.api.response.SpotsResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;
import rx.schedulers.Schedulers;

public class RestClient {

	private static SpotterApi apiService;

	private static SpotterApi createService(String baseUrl) {
		final OkHttpClient.Builder builder = new OkHttpClient.Builder()
			.addNetworkInterceptor(chain -> {
				final Request.Builder ongoing = chain.request().newBuilder();
				ongoing.addHeader("Content-Type", "application/json");
				return chain.proceed(ongoing.build());
			});

		if (BuildConfig.DEBUG) {
			final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor()
				.setLevel(HttpLoggingInterceptor.Level.BODY);
			builder.addInterceptor(httpLoggingInterceptor);
		}

		return getRetrofit(baseUrl, builder.build()).create(SpotterApi.class);
	}

	private static Retrofit getRetrofit(String baseUrl, OkHttpClient okHttpClient) {
		final Gson gson = new GsonBuilder()
			.registerTypeAdapter(Date.class, new DateDeserializer())
			.create();

		final Retrofit.Builder builder = new Retrofit.Builder()
			.addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
			.addConverterFactory(GsonConverterFactory.create(gson));

		return builder.client(okHttpClient).baseUrl(baseUrl).build();
	}

	public static SpotterApi getService(Context context) {
		if (apiService == null) {
			apiService = createService(context.getString(R.string.api_host));
		}
		return apiService;
	}

	public interface SpotterApi {

		@GET("categories/")
		Observable<CategoryResponse> getCategories();

		@GET("map/spots/")
		Observable<SpotsResponse> getSpots(@QueryMap(encoded = true) Map<String, String> params);

	}

}