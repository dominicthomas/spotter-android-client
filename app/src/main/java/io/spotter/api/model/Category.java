package io.spotter.api.model;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Category extends RealmObject implements RealmModel {

	@PrimaryKey
	private String id;

	private String name;

	private int depth;

	private String parentId;

	private RealmList<Category> children;

	private boolean enabled = true; // Default

	public Category() {
		// Empty
	}

	public Category(String id, String name, int depth, String parentId, RealmList<Category> children, boolean enabled) {
		this.id = id;
		this.name = name;
		this.depth = depth;
		this.parentId = parentId;
		this.children = children;
		this.enabled = enabled;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getDepth() {
		return depth;
	}

	public String getParentId() {
		return parentId;
	}

	public RealmList<Category> getChildren() {
		return children;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public void setChildren(RealmList<Category> children) {
		this.children = children;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
