package io.spotter.api.model;

import io.realm.RealmObject;

public class ChangedOn extends RealmObject {

	private Integer low;

	private Integer high;

	public Integer getLow() {
		return low;
	}

	public void setLow(Integer low) {
		this.low = low;
	}

	public Integer getHigh() {
		return high;
	}

	public void setHigh(Integer high) {
		this.high = high;
	}

}
