package io.spotter.api.model;

import io.realm.RealmObject;

public class HasProfileImage extends RealmObject {

	private Image image;

	private ChangedOn changedOn;

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public ChangedOn getChangedOn() {
		return changedOn;
	}

	public void setChangedOn(ChangedOn changedOn) {
		this.changedOn = changedOn;
	}

}