package io.spotter.api.model;

import io.realm.RealmObject;

public class Image extends RealmObject {

	private Integer width;

	private Integer height;

	private String href;

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

}
