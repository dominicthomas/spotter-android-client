package io.spotter.api.model;

import io.realm.RealmObject;

public class LocatedAt extends RealmObject {

	private Location location;

	private String accuracy;

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

}
