package io.spotter.api.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Spot extends RealmObject {

	@PrimaryKey
	private String id;

	private String description;

	private Long ttl;

	private String timestamp;

	private Visibility visibility;

	private LocatedAt locatedAt;

	private Owner owner;

	private Category category;

	private String imageUrl;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTtl() {
		return ttl;
	}

	public void setTtl(Long ttl) {
		this.ttl = ttl;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	public LocatedAt getLocatedAt() {
		return locatedAt;
	}

	public void setLocatedAt(LocatedAt locatedAt) {
		this.locatedAt = locatedAt;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
