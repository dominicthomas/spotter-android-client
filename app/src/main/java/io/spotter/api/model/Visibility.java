package io.spotter.api.model;

import io.realm.RealmObject;

public class Visibility extends RealmObject {

	private String mode;

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
}
