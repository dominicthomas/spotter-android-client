package io.spotter.api.response;

import io.realm.RealmList;
import io.realm.RealmObject;

public abstract class BaseResponse<T extends RealmObject> {

	private String status;

	private RealmList<T> data;

	public BaseResponse() {
		// Empty
	}

	public BaseResponse(String status, RealmList<T> data) {
		this.status = status;
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public RealmList<T> getData() {
		return data;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setData(RealmList<T> data) {
		this.data = data;
	}

}
