package io.spotter.api.response;

import io.realm.RealmList;
import io.spotter.api.model.Category;

public class CategoryResponse extends BaseResponse<Category> {

	public CategoryResponse() {
		// Empty
	}

	public CategoryResponse(String status, RealmList<Category> data) {
		super(status, data);
	}
}
