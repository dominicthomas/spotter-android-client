package io.spotter.api.response;

import io.realm.RealmList;
import io.spotter.api.model.Spot;

public class SpotsResponse extends BaseResponse<Spot> {

	public SpotsResponse() {
		// Empty
	}

	public SpotsResponse(String status, RealmList<Spot> data) {
		super(status, data);
	}
}
