package io.spotter.app.mvp;

public interface ActivityPresenter {

	void onStart();

	void onRestart();

	void onResume();

	void onPause();

	void onStop();

	void onDestroy();
}
