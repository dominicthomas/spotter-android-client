package io.spotter.app.mvp;


public abstract class BaseActivityPresenter<V, I> extends BasePresenter<V, I> implements ActivityPresenter {

	public BaseActivityPresenter(V view, I interactor) {
		super(view, interactor);
	}

	@Override
	public void onStart() {
		// NOOP
	}

	@Override
	public void onRestart() {
		// NOOP
	}

	@Override
	public void onResume() {
		// NOOP
	}

	@Override
	public void onPause() {
		// NOOP
	}

	@Override
	public void onStop() {
		// NOOP
	}

	@Override
	public void onDestroy() {
		// NOOP
	}
}
