package io.spotter.app.mvp;

import io.realm.Realm;
import io.spotter.api.RestClient;

public abstract class BaseInteractor {

	final RestClient.SpotterApi restClient;

	public BaseInteractor(RestClient.SpotterApi restClient) {
		this.restClient = restClient;
	}

	public RestClient.SpotterApi getRestClient() {
		return restClient;
	}

	public abstract Realm getDataBase();

}
