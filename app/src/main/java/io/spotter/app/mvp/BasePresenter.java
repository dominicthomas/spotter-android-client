package io.spotter.app.mvp;


public abstract class BasePresenter<V, I> {

	private final V view;

	private final I interactor;

	public BasePresenter(V view, I interactor) {
		this.view = view;
		this.interactor = interactor;
	}

	public V getView() {
		return view;
	}

	public I getInteractor() {
		return interactor;
	}

}
