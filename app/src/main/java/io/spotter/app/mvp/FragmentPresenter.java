package io.spotter.app.mvp;

public interface FragmentPresenter {

	void onStart();

	void onStop();

}
