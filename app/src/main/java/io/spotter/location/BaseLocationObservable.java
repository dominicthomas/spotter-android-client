package io.spotter.location;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.Arrays;
import java.util.List;

import io.spotter.location.error.GoogleAPIConnectionException;
import io.spotter.location.error.GoogleAPIConnectionSuspendedException;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.subscriptions.Subscriptions;

public abstract class BaseLocationObservable<T> implements Observable.OnSubscribe<T> {

	private final Context context;

	private final List<Api<? extends Api.ApiOptions.NotRequiredOptions>> services;

	protected BaseLocationObservable(Context context, Api<? extends Api.ApiOptions.NotRequiredOptions>... services) {
		this.context = context;
		this.services = Arrays.asList(services);
	}

	@Override
	public void call(Subscriber<? super T> subscriber) {
		final GoogleApiClient apiClient = createApiClient(subscriber);
		try {
			apiClient.connect();
		} catch (Throwable ex) {
			subscriber.onError(ex);
		}
		subscriber.add(Subscriptions.create(() -> {
			if (apiClient.isConnected() || apiClient.isConnecting()) {
				onUnsubscribe(apiClient);
				apiClient.disconnect();
			}
		}));
	}

	protected GoogleApiClient createApiClient(Subscriber<? super T> subscriber) {

		final ApiClientConnectionCallbacks apiClientConnectionCallbacks = new ApiClientConnectionCallbacks(subscriber);

		final GoogleApiClient.Builder apiClientBuilder = new GoogleApiClient.Builder(context);
		apiClientBuilder.addApi(LocationServices.API); // Currently only supports location api
		apiClientBuilder.addConnectionCallbacks(apiClientConnectionCallbacks);
		apiClientBuilder.addOnConnectionFailedListener(apiClientConnectionCallbacks);

		final GoogleApiClient apiClient = apiClientBuilder.build();
		apiClientConnectionCallbacks.setClient(apiClient);

		return apiClient;
	}

	protected abstract void onGoogleApiClientReady(GoogleApiClient apiClient, Observer<? super T> observer);

	protected void onUnsubscribe(GoogleApiClient locationClient) {
	}

	private class ApiClientConnectionCallbacks implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {

		final private Observer<? super T> observer;

		private GoogleApiClient apiClient;

		private ApiClientConnectionCallbacks(Observer<? super T> observer) {
			this.observer = observer;
		}

		@Override
		public void onConnected(Bundle bundle) {
			try {
				onGoogleApiClientReady(apiClient, observer);
			} catch (Throwable ex) {
				observer.onError(ex);
			}
		}

		@Override
		public void onConnectionSuspended(int cause) {
			observer.onError(new GoogleAPIConnectionSuspendedException(cause));
		}

		@Override
		public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
			observer.onError(new GoogleAPIConnectionException("Error connecting to GoogleApiClient.", connectionResult));
		}

		public void setClient(GoogleApiClient client) {
			this.apiClient = client;
		}
	}

}