package io.spotter.location;

import android.content.Context;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

import rx.Observable;
import rx.Observer;

public class GoogleAPIClientObservable extends BaseLocationObservable<GoogleApiClient> {

	@SafeVarargs
	public static Observable<GoogleApiClient> create(Context context, Api<? extends Api.ApiOptions.NotRequiredOptions>... apis) {
		return Observable.create(new GoogleAPIClientObservable(context, apis));
	}

	@SafeVarargs
	protected GoogleAPIClientObservable(Context context, Api<? extends Api.ApiOptions.NotRequiredOptions>... apis) {
		super(context, apis);
	}

	@Override
	protected void onGoogleApiClientReady(GoogleApiClient apiClient, Observer<? super GoogleApiClient> observer) {
		observer.onNext(apiClient);
		observer.onCompleted();
	}
}