package io.spotter.location;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import rx.Observable;
import rx.Observer;

/**
 * Provides the last known location immediately
 */
public class LastKnownLocationObservable extends BaseLocationObservable<Location> {

	public static Observable<Location> create(Context context) {
		return Observable.create(new LastKnownLocationObservable(context));
	}

	private LastKnownLocationObservable(Context context) {
		super(context);
	}

	@Override
	@SuppressWarnings("MissingPermission")
	protected void onGoogleApiClientReady(GoogleApiClient apiClient, Observer<? super Location> observer) {
		final Location location = LocationServices.FusedLocationApi.getLastLocation(apiClient);
		if (location != null) {
			observer.onNext(location); // Money
		}
		observer.onCompleted();
	}

}