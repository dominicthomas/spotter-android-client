package io.spotter.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.RequiresPermission;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

import rx.Observable;
import rx.functions.Func1;

/**
 * Factory of observables that can manipulate location
 * delivered by Google Play Services...
 * <p>
 * Based on: https://github.com/mcharmas/Android-ReactiveLocation
 */
public class LocationProvider {

	private final Context context;

	public LocationProvider(Context context) {
		this.context = context;
	}

	/**
	 * Creates observable that obtains last known location and than completes.
	 * Delivered location is never null - when it is unavailable Observable completes without emitting
	 * any value.
	 *
	 * @return observable that serves last know location
	 */
	@RequiresPermission(anyOf = {
		"android.permission.ACCESS_COARSE_LOCATION",
		"android.permission.ACCESS_FINE_LOCATION"}
	)
	public Observable<Location> getLastKnownLocation() {
		return LastKnownLocationObservable.create(context);
	}

	/**
	 * Creates observable that allows to observe infinite stream of location updates.
	 * To stop the stream you have to unsubscribe from observable - location updates are
	 * then disconnected.
	 *
	 * @param locationRequest request object with info about what kind of location you need
	 * @return observable that serves infinite stream of location updates
	 */
	@RequiresPermission(anyOf = {
		"android.permission.ACCESS_COARSE_LOCATION",
		"android.permission.ACCESS_FINE_LOCATION"}
	)
	public Observable<Location> getUpdatedLocation(LocationRequest locationRequest) {
		return LocationUpdatesObservable.create(context, locationRequest);
	}

	/**
	 * Observable that can be used to check settings state for given location request.
	 *
	 * @param locationRequest location request
	 * @return observable that emits check result of location settings
	 * @see com.google.android.gms.location.SettingsApi
	 */
	public Observable<LocationSettingsResult> checkLocationSettings(final LocationSettingsRequest locationRequest) {
		return getGoogleApiClientObservable(LocationServices.API)
			.flatMap(new Func1<GoogleApiClient, Observable<LocationSettingsResult>>() {
				@Override
				public Observable<LocationSettingsResult> call(GoogleApiClient googleApiClient) {
					return fromPendingResult(LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationRequest));
				}
			});
	}

	/**
	 * Observable that emits {@link com.google.android.gms.common.api.GoogleApiClient} object after connection.
	 * <p>
	 * Do not disconnect from apis client manually - just unsubscribe.
	 *
	 * @param apis collection of apis to connect to
	 * @return observable that emits apis client after successful connection
	 */
	public Observable<GoogleApiClient> getGoogleApiClientObservable(Api... apis) {
		//noinspection unchecked
		return GoogleAPIClientObservable.create(context, apis);
	}

	/**
	 * Util method that wraps {@link com.google.android.gms.common.api.PendingResult} in Observable.
	 *
	 * @param result pending result to wrap
	 * @param <T>    parameter type of result
	 * @return observable that emits pending result and completes
	 */
	public static <T extends Result> Observable<T> fromPendingResult(PendingResult<T> result) {
		return Observable.create(new PendingResultObservable<>(result));
	}

	/**
	 * Check that the users's gps provider is enabled
	 * @param context
	 * @return
	 */
	public static boolean isLocationEnabled(Context context) {
		final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}
}
