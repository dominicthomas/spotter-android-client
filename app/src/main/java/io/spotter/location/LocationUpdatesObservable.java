package io.spotter.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import rx.Observable;
import rx.Observer;

public class LocationUpdatesObservable extends BaseLocationObservable<Location> {

	private final LocationRequest locationRequest;

	private LocationListener listener;

	public static Observable<Location> create(Context context, LocationRequest locationRequest) {
		return Observable.create(new LocationUpdatesObservable(context, locationRequest));
	}

	private LocationUpdatesObservable(Context context, LocationRequest locationRequest) {
		super(context);
		this.locationRequest = locationRequest;
	}

	@Override
	@SuppressWarnings("MissingPermission")
	protected void onGoogleApiClientReady(GoogleApiClient apiClient, final Observer<? super Location> observer) {
		listener = observer::onNext; // Money
		LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationRequest, listener);
	}

	@Override
	protected void onUnsubscribe(GoogleApiClient locationClient) {
		if (locationClient.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(locationClient, listener);
		}
	}

}
