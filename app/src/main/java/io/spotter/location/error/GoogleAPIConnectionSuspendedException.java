package io.spotter.location.error;

public class GoogleAPIConnectionSuspendedException extends RuntimeException {
	private final int cause;

	public GoogleAPIConnectionSuspendedException(int cause) {
		this.cause = cause;
	}

	public int getErrorCause() {
		return cause;
	}
}