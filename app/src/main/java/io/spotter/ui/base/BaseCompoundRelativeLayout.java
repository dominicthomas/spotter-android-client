package io.spotter.ui.base;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import butterknife.ButterKnife;

public abstract class BaseCompoundRelativeLayout extends RelativeLayout {

	public BaseCompoundRelativeLayout(Context context) {
		this(context, null);
	}

	public BaseCompoundRelativeLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public BaseCompoundRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		inflate(context, getLayoutToInflate(), this);
		ButterKnife.bind(this);
		initialise();
	}

	@CallSuper
	protected void initialise() {
		// NOOP
	}

	protected abstract int getLayoutToInflate();

}
