package io.spotter.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import io.spotter.R;
import io.spotter.app.mvp.ActivityPresenter;

public abstract class BaseMVPActivity<P extends ActivityPresenter> extends BaseActivity {

	private P presenter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		setTheme(R.style.AppTheme);
		super.onCreate(savedInstanceState);
		presenter = initPresenter();
	}

	public abstract P initPresenter();

	public P getPresenter() {
		return presenter;
	}

	@Override
	protected void onStart() {
		super.onStart();
		presenter.onStart();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		presenter.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		presenter.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		presenter.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		presenter.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		presenter.onDestroy();
	}

}
