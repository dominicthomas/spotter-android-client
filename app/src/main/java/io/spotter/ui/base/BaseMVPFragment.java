package io.spotter.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import io.realm.Realm;
import io.spotter.app.mvp.FragmentPresenter;

public abstract class BaseMVPFragment<P extends FragmentPresenter> extends Fragment {

	private P presenter;

	private boolean isFragmentLoaded = false;

	private Realm realmInstance;

	public BaseMVPFragment() {
		// Empty constructor
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		realmInstance = Realm.getDefaultInstance();
		/*
			Realm.init(context);
			realm = Realm.getInstance(new RealmConfiguration.Builder()
			.schemaVersion(BuildConfig.REALM_SCHEMA_VERSION)
			.deleteRealmIfMigrationNeeded()
			.build());
		 */
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(getLayoutId(), container, false);
		ButterKnife.bind(this, view);
		presenter = initPresenter();
		return view;
	}

	public abstract int getLayoutId();

	protected abstract P initPresenter();

	public P getPresenter() {
		return presenter;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser && !isFragmentLoaded) {
			isFragmentLoaded = true;
		}
	}

	protected boolean isFragmentVisible() {
		return isFragmentLoaded;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (presenter == null) return;
		if (!isVisible()) return;
		presenter.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
		if (presenter == null) return;
		presenter.onStop();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		realmInstance.close();
	}

	// TODO: Move to base?
	public Realm getDataBase() {
		return realmInstance;
	}

}
