package io.spotter.ui.categories;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import io.spotter.api.model.Category;

public class CategoriesAdapter extends RealmRecyclerViewAdapter<Category, CategoriesAdapter.CategoryViewHolder> {

	private final CategoryContract.Presenter presenter;

	public CategoriesAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<Category> data,
	                         boolean autoUpdate, CategoryContract.Presenter presenter) {
		super(context, data, autoUpdate);
		this.presenter = presenter;
	}

	@Override
	public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		final View itemView = new CategoryItemView(parent.getContext());
		return new CategoryViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(CategoryViewHolder categoryViewHolder, int position) {
		if (getData() == null) return;
		final Category category = getData().get(position);
		final CategoryItemView categoryItemView = (CategoryItemView) categoryViewHolder.itemView;
		categoryItemView.setData(category, categoryViewHolder);
	}

	class CategoryViewHolder extends RecyclerView.ViewHolder implements CategoryItemView.CategoryItemListener {

		CategoryViewHolder(View itemView) {
			super(itemView);
		}

		@Override
		public void onCheckChanged(Category category, boolean isChecked) {
			presenter.onSwitchChecked(category, isChecked);
		}
	}
}
