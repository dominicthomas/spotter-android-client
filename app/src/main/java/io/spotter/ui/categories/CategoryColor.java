package io.spotter.ui.categories;


import com.google.android.gms.maps.model.BitmapDescriptorFactory;

/**
 * This is just for the MVP - colour should be set by the api
 */
public enum CategoryColor {

	CULTURAL(BitmapDescriptorFactory.HUE_AZURE),
	ENTERTAINMENT(BitmapDescriptorFactory.HUE_CYAN),
	EVENTS(BitmapDescriptorFactory.HUE_GREEN),
	SHARING(BitmapDescriptorFactory.HUE_ORANGE),
	SHOPPING(BitmapDescriptorFactory.HUE_VIOLET),
	SPORTS(BitmapDescriptorFactory.HUE_YELLOW);

	private float color;

	CategoryColor(float color) {
		this.color = color;
	}

	public float getColor() {
		return color;
	}
}
