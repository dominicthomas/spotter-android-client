package io.spotter.ui.categories;


import com.fernandocejas.arrow.optional.Optional;

import io.spotter.api.model.Category;

public interface CategoryContract {

	interface View {

		void setData(Category category, CategoryItemView.CategoryItemListener categoryItemListener);

		Optional<Category> getData();
	}

	interface Presenter {
		void onSwitchChecked(Category category, boolean isChecked);
	}

}
