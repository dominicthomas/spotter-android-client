package io.spotter.ui.categories;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fernandocejas.arrow.optional.Optional;

import butterknife.BindView;
import io.spotter.R;
import io.spotter.api.model.Category;
import io.spotter.ui.base.BaseCompoundRelativeLayout;
import io.spotter.ui.views.ColorSwitch;
import io.spotter.utils.CategoryHelper;

public class CategoryItemView extends BaseCompoundRelativeLayout implements
	CategoryContract.View, CompoundButton.OnCheckedChangeListener {

	@BindView(R.id.category_name)
	TextView categoryName;

	@BindView(R.id.category_switch)
	ColorSwitch categorySwitch;

	@BindView(R.id.category_children)
	LinearLayout childrenContainer;

	private Optional<Category> category = Optional.absent();

	private CategoryItemListener categoryItemListener;

	public interface CategoryItemListener {
		void onCheckChanged(Category category, boolean isChecked);
	}

	public CategoryItemView(Context context) {
		super(context);
	}

	public CategoryItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CategoryItemView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected int getLayoutToInflate() {
		return R.layout.layout_category_item;
	}

	@Override
	protected void initialise() {
		super.initialise();
		categorySwitch.setOnCheckedChangeListener(this);
	}

	@Override
	public void setData(Category category, CategoryItemListener categoryItemListener) {
		this.category = Optional.fromNullable(category);
		this.categoryItemListener = categoryItemListener;

		// Setup view
		categoryName.setText(category.getName());
		categorySwitch.setPrimaryColor(CategoryHelper.getColor(category));
		categorySwitch.setChecked(category.isEnabled());

		// Child categories?
		childrenContainer.removeAllViews();
		if (category.isEnabled() && !category.getChildren().isEmpty()) {
			for (Category childCategory : category.getChildren()) {
				final CategoryItemView categoryItemView = new CategoryItemView(getContext());
				categoryItemView.setData(childCategory, categoryItemListener);
				childrenContainer.addView(categoryItemView);
			}
		}
	}

	@Override
	public Optional<Category> getData() {
		return category;
	}

	@Override
	public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
		if (category.isPresent() && categoryItemListener != null) {
			categoryItemListener.onCheckChanged(category.get(), isChecked);
		}
	}

}
