package io.spotter.ui.feed;

import io.spotter.R;
import io.spotter.ui.base.BaseMVPFragment;
import io.spotter.app.mvp.FragmentPresenter;

public class FeedFragment extends BaseMVPFragment {

	@Override
	public int getLayoutId() {
		return R.layout.fragment_feed;
	}

	@Override
	protected FragmentPresenter initPresenter() {
		return null;
	}

}
