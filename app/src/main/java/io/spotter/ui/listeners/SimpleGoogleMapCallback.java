package io.spotter.ui.listeners;


import com.google.android.gms.maps.GoogleMap;

public abstract class SimpleGoogleMapCallback implements GoogleMap.CancelableCallback {

	public abstract void onFinished();

	@Override
	public void onFinish() {
		onFinished();
	}

	@Override
	public void onCancel() {
		// NOOP
	}

}
