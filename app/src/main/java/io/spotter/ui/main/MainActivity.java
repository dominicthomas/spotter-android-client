package io.spotter.ui.main;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import butterknife.BindView;
import io.spotter.R;
import io.spotter.ui.base.BaseMVPActivity;
import io.spotter.ui.feed.FeedFragment;
import io.spotter.ui.map.MapFragment;
import io.spotter.ui.notifications.NotificationsFragment;

import static io.spotter.ui.main.MainActivity.Tabs.FEED;
import static io.spotter.ui.main.MainActivity.Tabs.MAP;
import static io.spotter.ui.main.MainActivity.Tabs.NOTIFICATIONS;
import static io.spotter.utils.PermissionsHelper.LOCATION_PERMISSION_REQUEST;

public class MainActivity extends BaseMVPActivity<MainContract.Presenter> implements MainContract.View {

	public enum Tabs { // Max is 5
		FEED, MAP, NOTIFICATIONS
	}

	private String currentFragmentTag;

	@BindView(R.id.bottom_navigation)
	BottomNavigationView bottomNavigationView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getPresenter().initBottomNavigation(bottomNavigationView,
			R.menu.bottom_navigation_main, R.id.action_feed);
	}

	@Override
	public MainContract.Presenter initPresenter() {
		return new MainPresenter(this, null);
	}

	@Override
	public int getLayoutId() {
		return R.layout.activity_main;
	}

	@Override
	protected void onStart() {
		super.onStart();
		bottomNavigationView.setEnabled(getPresenter().hasLocationPermissions(this));
	}

	@Override
	public boolean onTabItemClicked(int itemId) {
		if (!getPresenter().hasLocationPermissions(this)) {
			return false;
		}
		switch (itemId) {
			case R.id.action_feed:
				loadFragment(new FeedFragment(), FEED.name());
				break;
			case R.id.action_map:
				loadFragment(new MapFragment(), MAP.name());
				break;
			case R.id.action_notification:
				loadFragment(new NotificationsFragment(), NOTIFICATIONS.name());
				break;
		}
		return true;
	}

	private void loadFragment(Fragment fragment, String tag) {
		final FragmentManager supportFragmentManager = getSupportFragmentManager();
		if (supportFragmentManager.findFragmentByTag(tag) != null) return;
		final FragmentTransaction transaction = supportFragmentManager.beginTransaction();
		transaction.replace(R.id.main_container, fragment, tag).commit();
		currentFragmentTag = tag;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case LOCATION_PERMISSION_REQUEST: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(this, "Granted!", Toast.LENGTH_SHORT).show(); // TODO: Proceed with loading!

					// TODO: Helper method
					bottomNavigationView.setEnabled(getPresenter().hasLocationPermissions(this));
					this.onTabItemClicked(R.id.action_feed);
				} else {
					Toast.makeText(this, "Denied!", Toast.LENGTH_SHORT).show(); // TODO: Show rationale?
				}
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		final Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
		if (fragmentByTag == null) return;
		fragmentByTag.onActivityResult(requestCode, resultCode, data);
	}
}
