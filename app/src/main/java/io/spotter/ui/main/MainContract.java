package io.spotter.ui.main;

import android.app.Activity;
import android.support.design.widget.BottomNavigationView;

import io.spotter.app.mvp.ActivityPresenter;

public interface MainContract {

	interface View {

		boolean onTabItemClicked(int itemId);
	}

	interface Presenter extends ActivityPresenter {

		void initBottomNavigation(BottomNavigationView bottomNavigationView, int tabsMenuLayout, int tabIdToSelect);

		boolean hasLocationPermissions(Activity activity);
	}

}
