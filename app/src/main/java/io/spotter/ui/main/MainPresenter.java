package io.spotter.ui.main;

import android.app.Activity;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;

import io.spotter.app.mvp.BaseActivityPresenter;
import io.spotter.ui.map.MapContract;
import io.spotter.utils.LocationHelper;
import io.spotter.utils.PermissionsHelper;
import timber.log.Timber;

import static io.spotter.utils.LocationHelper.GOOGLE_PLAY_SERVICES_REQUEST;
import static io.spotter.utils.PermissionsHelper.LOCATION_PERMISSION_REQUEST;

class MainPresenter extends BaseActivityPresenter<MainContract.View, MapContract.Interactor> implements MainContract.Presenter {

	MainPresenter(MainContract.View view, MapContract.Interactor interactor) {
		super(view, interactor);
	}

	@Override
	public void initBottomNavigation(BottomNavigationView bottomNavigationView, int tabsMenuLayout, int tabIdToSelect) {
		bottomNavigationView.inflateMenu(tabsMenuLayout);
		bottomNavigationView.setOnNavigationItemSelectedListener(
			item -> getView().onTabItemClicked(item.getItemId()));
		bottomNavigationView.setEnabled(true);
		getView().onTabItemClicked(tabIdToSelect);
	}

	@Override
	public boolean hasLocationPermissions(Activity activity) {
		if (!PermissionsHelper.hasLocationPermissions(activity)) {
			PermissionsHelper.requestLocationPermissions(activity, LOCATION_PERMISSION_REQUEST);
		} else if (LocationHelper.isGooglePlayServicesAvailable(activity, GOOGLE_PLAY_SERVICES_REQUEST)) {
			Timber.d("Google play services loaded!");
			return true;
		}
		return false;
	}

}
