package io.spotter.ui.map;


import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.fernandocejas.arrow.optional.Optional;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;


import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import io.spotter.api.model.Category;
import io.spotter.api.model.Spot;
import io.spotter.api.response.CategoryResponse;
import io.spotter.api.response.SpotsResponse;
import io.spotter.app.mvp.FragmentPresenter;
import io.spotter.ui.listeners.SimpleGoogleMapCallback;
import io.spotter.ui.map.markers.SpotMarker;
import io.spotter.utils.CategoryHelper;
import rx.Observable;

public interface MapContract {

	interface View {

		void onCategoriesLoaded(List<Category> categories);

		void onCurrentLocationAcquired(LatLng currentLatLng);

		void showLoading(boolean isLoading);

		void onError(Throwable throwable);

		void onLocationIsNotEnabled();

		void onMapMovedByGesture();

		void onLocationButtonClicked();

		void onSpotInfoWindowClicked(Marker marker);

		void onMapMovedPastPreviousRadius(LatLng currentPosition);

		void renderSpots(List<Spot> spotList, boolean isAtMinZoomLevel);

		void clearSpots();

		void onCameraPositionChanged(CameraPosition cameraPosition);

		interface Renderer {

			@Deprecated
			void addSpotToMap(SpotMarker spotMarker);

			void onCurrentLocationAcquired(LatLng currentLatLng, SimpleGoogleMapCallback simpleGoogleMapCallback);

			void onMapMovedByGesture();

			CameraPosition getCameraPosition();

			void centerMapToLocation(LatLng currentLatLng, SimpleGoogleMapCallback simpleGoogleMapCallback);

			void renderSpots(List<Spot> spotList, boolean isAtMinZoomLevel);

			void clearSpots(Activity activity);

			void clearSubscriptions();

			Observable<String> getMarkerId(Marker marker);
		}
	}

	interface Presenter extends FragmentPresenter {

		void loadCategoriesIntoList(Context context, RecyclerView categoriesList);

		void toggleCategoryDrawer(boolean drawerOpen, DrawerLayout categoriesDrawerLayout);

		void toggleCategoryIcon(Context context, boolean drawerOpen, MenuItem editCategoriesItem);

		void checkCurrentLocation(Context context);

		void getCategories(Context context);

		void getSpots();

		@SuppressWarnings("MissingPermission")
		void getSpots(Observable<Location> locationObservable);

		void setMapInteractionListeners(GoogleMap googleMap);

		void showSpotInfoScreen(String id);

		void onCameraPositionChanged(CameraPosition cameraPosition);

		void checkLocationIsEnabled(Activity activity);
	}

	interface Interactor {

		Observable<List<Category>> getCategories();

		void saveCategories(CategoryResponse categoryResponse);

		Observable<List<Category>> getSavedCategories();

		Observable<List<Category>> getEnabledCategories();

		Observable<List<String>> getEnabledCategoryNames(CategoryHelper categoryHelper);

		Observable<List<String>> getEnabledCategoryIds(CategoryHelper categoryHelper);

		Observable<SpotsResponse> getSpots(Map<String, String> params);

		void clearCategories(Realm.Transaction.OnSuccess onSuccessCallback);

		RealmResults<Category> getLocalCategories();

		void setCategoryChecked(Category category, boolean isChecked);

		void saveSpotRequestLocation(LatLng latLng);

		Optional<LatLng> getSpotRequestLocation();
	}
}
