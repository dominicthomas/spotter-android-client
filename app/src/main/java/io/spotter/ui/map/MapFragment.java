package io.spotter.ui.map;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import butterknife.BindView;
import io.spotter.R;
import io.spotter.api.RestClient;
import io.spotter.api.model.Category;
import io.spotter.api.model.Spot;
import io.spotter.location.LastKnownLocationObservable;
import io.spotter.location.LocationProvider;
import io.spotter.ui.base.BaseMVPFragment;
import io.spotter.ui.listeners.SimpleGoogleMapCallback;
import io.spotter.utils.CategoryHelper;
import io.spotter.utils.MapUtils;
import io.spotter.utils.PermissionsHelper;
import io.spotter.utils.PreferencesHelper;
import rx.Observable;

import static com.google.android.gms.maps.model.MapStyleOptions.loadRawResourceStyle;
import static io.spotter.AppConstants.Map.MAP_STATE_BUNDLE_KEY;
import static io.spotter.AppConstants.Map.REQUEST_CHECK_LOCATION_SETTINGS;

public class MapFragment extends BaseMVPFragment<MapContract.Presenter>
	implements MapContract.View, OnMapReadyCallback {

	@BindView(R.id.map_view)
	MapView mapView;

	@BindView(R.id.drawer_layout)
	DrawerLayout categoriesDrawer;

	@BindView(R.id.drawer_list)
	RecyclerView categoriesList;

	private MenuItem editCategoriesItem;

	private MapRenderer mapRenderer;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState);
		mapView.getMapAsync(this);
		final Bundle mapState = savedInstanceState != null ?
			savedInstanceState.getBundle(MAP_STATE_BUNDLE_KEY) : null;
		mapView.onCreate(mapState);
		return view;
	}

	@Override
	@SuppressWarnings("MissingPermission")
	public void onMapReady(GoogleMap googleMap) {
		if (PermissionsHelper.hasLocationPermissions(getActivity())) {

			// Initialise map renderer
			mapRenderer = MapRenderer.getInstance(getActivity(), googleMap);

			// Setup map
			googleMap.setMyLocationEnabled(true);
			googleMap.setBuildingsEnabled(false);
			googleMap.getUiSettings().setTiltGesturesEnabled(false);
			googleMap.getUiSettings().setCompassEnabled(true);
			googleMap.getUiSettings().setRotateGesturesEnabled(true);
			googleMap.getUiSettings().setIndoorLevelPickerEnabled(true);
			googleMap.getUiSettings().setMapToolbarEnabled(false);
			googleMap.getUiSettings().setRotateGesturesEnabled(false);
			googleMap.setMapStyle(loadRawResourceStyle(getActivity(), R.raw.style_maps_retro_few_pois));

			// Map listeners
			getPresenter().setMapInteractionListeners(googleMap);

			// Get location
			getPresenter().checkCurrentLocation(getActivity());

		}
	}

	@Override
	@SuppressWarnings("ConstantConditions")
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initCategoriesDrawerLayout();
	}

	private void initCategoriesDrawerLayout() {
		getPresenter().loadCategoriesIntoList(getActivity(), categoriesList);
		categoriesDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		categoriesDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				// Empty
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				getPresenter().toggleCategoryIcon(getActivity(),
					categoriesDrawer.isDrawerOpen(categoriesList),
					editCategoriesItem);
				categoriesDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				getPresenter().toggleCategoryIcon(getActivity(),
					categoriesDrawer.isDrawerOpen(categoriesList),
					editCategoriesItem);
				getPresenter().getSpots();
				categoriesDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
			}

			@Override
			public void onDrawerStateChanged(int newState) {
				// Empty
			}
		});
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		final Bundle mapState = new Bundle();
		mapView.onSaveInstanceState(mapState);
		outState.putBundle(MAP_STATE_BUNDLE_KEY, mapState);
		super.onSaveInstanceState(outState);
	}

	@Override
	public int getLayoutId() {
		return R.layout.fragment_map;
	}

	@Override
	protected MapContract.Presenter initPresenter() {
		return new MapPresenter(this,
			new MapInteractor(RestClient.getService(getActivity()), PreferencesHelper.with(getActivity())),
			new LocationProvider(getActivity().getApplicationContext()),
			new CategoryHelper());
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.map_fragment_menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.edit_item:
				editCategoriesItem = item;
				getPresenter().toggleCategoryDrawer(
					categoriesDrawer.isDrawerOpen(categoriesList),
					categoriesDrawer
				);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		mapView.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		mapView.onPause();
		mapRenderer.clearSubscriptions();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}

	@Override
	public void onCurrentLocationAcquired(LatLng currentLatLng) {
		mapRenderer.onCurrentLocationAcquired(currentLatLng, new SimpleGoogleMapCallback() {
			@Override
			public void onFinished() {
				getPresenter().getCategories(getActivity());
			}
		});
	}

	@Override
	public void onCategoriesLoaded(List<Category> categories) {
		if (categories.isEmpty()) return;
		getPresenter().getSpots();
	}

	@Override
	public void showLoading(boolean isLoading) {
		// TODO
	}

	@Override
	public void onError(Throwable throwable) {
		// TODO: Custom error handling
	}

	@Override
	public void clearSpots() {
		mapRenderer.clearSpots(getActivity());
	}

	@Override
	public void onCameraPositionChanged(CameraPosition cameraPosition) {
		// NOOP
	}

	@Override
	public void onMapMovedByGesture() {
		getPresenter().onCameraPositionChanged(mapRenderer.getCameraPosition());
		mapRenderer.onMapMovedByGesture();
	}

	@Override
	public void onLocationButtonClicked() {
		final Observable<Location> locationObservable = LastKnownLocationObservable.create(getActivity());
		locationObservable.map(location -> new LatLng(location.getLatitude(), location.getLongitude()))
			.subscribe(latLng -> mapRenderer.centerMapToLocation(latLng, new SimpleGoogleMapCallback() {
				@Override
				public void onFinished() {
					getPresenter().getSpots();
				}
			}));
	}

	@Override
	public void onSpotInfoWindowClicked(Marker marker) {
		mapRenderer.getMarkerId(marker).subscribe(
			spotId -> getPresenter().showSpotInfoScreen(spotId));
	}

	@Override
	public void onMapMovedPastPreviousRadius(LatLng currentPosition) {
		final Observable<Location> locationObservable = MapUtils.createLocationObservable(currentPosition);
		getPresenter().getSpots(locationObservable);
	}

	@Override
	public void onLocationIsNotEnabled() {
		getPresenter().checkLocationIsEnabled(getActivity());
	}

	@Override
	public void renderSpots(List<Spot> spotList, boolean isAtMinZoomLevel) {
		if (spotList.isEmpty()) return;
		mapRenderer.renderSpots(spotList, isAtMinZoomLevel);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		switch (requestCode) {
			case REQUEST_CHECK_LOCATION_SETTINGS:
				switch (resultCode) {
					case Activity.RESULT_OK:
						getPresenter().checkCurrentLocation(getActivity());
						break;
					case Activity.RESULT_CANCELED:
						Toast.makeText(getActivity(), "Please enable your GPS and try again...", Toast.LENGTH_SHORT).show();
						break;
				}
				break;
		}
	}

}
