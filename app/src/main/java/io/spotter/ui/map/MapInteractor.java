package io.spotter.ui.map;


import com.fernandocejas.arrow.optional.Optional;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.spotter.api.RestClient;
import io.spotter.api.model.Category;
import io.spotter.api.response.CategoryResponse;
import io.spotter.api.response.SpotsResponse;
import io.spotter.app.mvp.BaseInteractor;
import io.spotter.utils.CategoryHelper;
import io.spotter.utils.PreferencesHelper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static io.spotter.AppConstants.Map.PREFS_KEY_REQUEST_LAT;
import static io.spotter.AppConstants.Map.PREFS_KEY_REQUEST_LON;

class MapInteractor extends BaseInteractor implements MapContract.Interactor {

	private final PreferencesHelper preferencesHelper;

	MapInteractor(RestClient.SpotterApi restClient, PreferencesHelper preferencesHelper) {
		super(restClient);
		this.preferencesHelper = preferencesHelper;
	}

	@Override
	public Realm getDataBase() {
		return Realm.getDefaultInstance();
	}

	@Override
	public Observable<List<Category>> getCategories() {
		return getRestClient().getCategories()
			.doOnNext(this::saveCategories)
			.flatMap(categories -> getSavedCategories())
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.doOnCompleted(() -> getDataBase().close());
	}

	@Override
	public void saveCategories(CategoryResponse categoryResponse) {
		final RealmList<Category> categoriesData = categoryResponse.getData();
		getDataBase().executeTransaction(realm -> realm.copyToRealmOrUpdate(categoriesData));
	}

	@Override
	public Observable<List<Category>> getSavedCategories() { // TODO: Try asObservable?
		final RealmResults<Category> categories = getLocalCategories();
		return Observable.from(categories).filter(category -> category.getDepth() == 0).toList();
	}

	@Override
	public Observable<List<Category>> getEnabledCategories() {
		final RealmResults<Category> categories = getLocalCategories();
		return Observable.from(categories).filter(Category::isEnabled).toList();
	}

	public Observable<List<String>> getEnabledCategoryNames(CategoryHelper categoryHelper) {
		final RealmResults<Category> categories = getLocalCategories();
		return Observable.from(categories)
			.filter(Category::isEnabled).toList()
			.map(categoryHelper::getEnabledCategoryNameList)
			.filter(categoryNames -> !categoryNames.isEmpty());
	}

	@Override
	public Observable<List<String>> getEnabledCategoryIds(CategoryHelper categoryHelper) {
		final RealmResults<Category> categories = getLocalCategories();
		return Observable.from(categories)
			.filter(Category::isEnabled).toList()
			.map(categoryHelper::getEnabledCategoryIdList)
			.filter(categoryIds -> !categoryIds.isEmpty());
	}

	@Override
	public Observable<SpotsResponse> getSpots(Map<String, String> params) {
		return getRestClient().getSpots(params) // TODO: Save spots.. overwrite last items
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread());
	}

	@Override
	public void clearCategories(Realm.Transaction.OnSuccess onSuccessCallback) {
		getDataBase().executeTransactionAsync(realm -> realm.delete(Category.class), onSuccessCallback);
	}

	@Override
	public RealmResults<Category> getLocalCategories() {
		return getDataBase().where(Category.class).equalTo("depth", 0).findAll().sort("name");
	}

	@Override
	public void setCategoryChecked(Category category, boolean isChecked) {
		getDataBase().executeTransaction(realm -> {

			final boolean isChildCategory = category.getDepth() > 0;
			final String categoryId = isChildCategory ? category.getParentId() : category.getId();
			final Category categoryToChange = getLocalCategories().where().contains("id", categoryId).findFirst();
			if (categoryToChange == null) return;

			// Child Category
			if (isChildCategory) {
				final Category childCategory = categoryToChange.getChildren().where().contains("id", category.getId()).findFirst();
				if (childCategory.isEnabled() == isChecked) return;
				childCategory.setEnabled(isChecked);
				realm.insertOrUpdate(categoryToChange);
				logCategoryChecked(childCategory);
				return;
			}

			// Main Category
			if (categoryToChange.isEnabled() == isChecked) return;
			categoryToChange.setEnabled(isChecked);
			if (!categoryToChange.getChildren().isEmpty()) {
				for (Category childCategory : categoryToChange.getChildren()) {
					childCategory.setEnabled(isChecked);
				}
			}
			realm.insertOrUpdate(categoryToChange);
			logCategoryChecked(category);
		});
	}

	@Override
	public void saveSpotRequestLocation(LatLng latLng) {
		preferencesHelper.save(PREFS_KEY_REQUEST_LAT, String.valueOf(latLng.latitude));
		preferencesHelper.save(PREFS_KEY_REQUEST_LON, String.valueOf(latLng.longitude));
	}

	@Override
	public Optional<LatLng> getSpotRequestLocation() {
		final boolean latLngPrefsExists = preferencesHelper.contains(PREFS_KEY_REQUEST_LAT)
			&& preferencesHelper.contains(PREFS_KEY_REQUEST_LON);
		if (!latLngPrefsExists) return Optional.absent();
		final String requestLat = preferencesHelper.getString(PREFS_KEY_REQUEST_LAT, null);
		final String requestLon = preferencesHelper.getString(PREFS_KEY_REQUEST_LON, null);
		final LatLng requestLatLng = new LatLng(Double.valueOf(requestLat), Double.valueOf(requestLon));
		return Optional.fromNullable(requestLatLng);
	}

	private void logCategoryChecked(Category category) {
		Timber.d(category.getName() + " / " + String.valueOf(category.isEnabled()));
	}

}
