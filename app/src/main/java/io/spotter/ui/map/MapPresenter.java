package io.spotter.ui.map;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.fernandocejas.arrow.collections.Lists;
import com.fernandocejas.arrow.strings.Strings;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.spotter.AppConstants;
import io.spotter.R;
import io.spotter.api.model.Category;
import io.spotter.api.model.Spot;
import io.spotter.api.response.SpotsResponse;
import io.spotter.app.mvp.BasePresenter;
import io.spotter.location.LocationProvider;
import io.spotter.ui.categories.CategoriesAdapter;
import io.spotter.utils.CategoryHelper;
import io.spotter.utils.LocationHelper;
import io.spotter.utils.MapUtils;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;
import timber.log.Timber;

import static android.support.v4.content.ContextCompat.getDrawable;
import static com.google.maps.android.SphericalUtil.computeDistanceBetween;
import static io.spotter.AppConstants.Map.FASTEST_UPDATE_INTERVAL;
import static io.spotter.AppConstants.Map.MIN_ZOOM_LEVEL_TO_LOAD_SPOTS;
import static io.spotter.AppConstants.Map.MIN_ZOOM_LEVEL_TO_RENDER_MARKER_THUMB;
import static io.spotter.AppConstants.Map.REQUEST_CHECK_LOCATION_SETTINGS;
import static io.spotter.AppConstants.Map.SHOULD_LOAD_FAKE_DATA;
import static io.spotter.AppConstants.Map.SPOTS_CALL_RADIUS;
import static io.spotter.AppConstants.Map.TEST_SPOTS_COUNT;
import static io.spotter.AppConstants.Map.UPDATE_INTERVAL;

class MapPresenter extends BasePresenter<MapContract.View, MapContract.Interactor> implements MapContract.Presenter,
	GoogleMap.OnCameraMoveStartedListener,
	GoogleMap.OnCameraMoveListener,
	GoogleMap.OnCameraMoveCanceledListener,
	GoogleMap.OnCameraIdleListener,
	GoogleMap.OnMyLocationButtonClickListener,
	GoogleMap.OnInfoWindowClickListener {

	private final LocationProvider locationProvider;

	private Subscription locationTrackerSubscription = Subscriptions.empty();

	private final CategoryHelper categoryHelper;

	private boolean isMapMoveReasonGesture = false;

	private final List<Spot> spotList = Lists.newArrayList();

	private CompositeSubscription compositeSubscription = new CompositeSubscription();

	private boolean isAtMinZoomLevelToRenderThumbs;

	MapPresenter(MapContract.View view, MapContract.Interactor interactor, LocationProvider locationProvider, CategoryHelper categoryHelper) {
		super(view, interactor);
		this.locationProvider = locationProvider;
		this.categoryHelper = categoryHelper;
	}

	@Override
	public void onStart() {
		// Empty
	}

	@Override
	public void onStop() {
		// Empty
	}

	@Override
	public void toggleCategoryDrawer(boolean drawerOpen, DrawerLayout categoriesDrawerLayout) {
		if (!drawerOpen) {
			categoriesDrawerLayout.openDrawer(GravityCompat.END);
		} else {
			categoriesDrawerLayout.closeDrawers();
		}
	}

	@Override
	public void toggleCategoryIcon(Context context, boolean drawerOpen, MenuItem editCategoriesItem) {
		if (!drawerOpen) {
			editCategoriesItem.setIcon(getDrawable(context, R.drawable.ic_edit_light));
		} else {
			editCategoriesItem.setIcon(getDrawable(context, R.drawable.ic_edit_accent));
		}
	}

	@Override
	public void loadCategoriesIntoList(Context context, RecyclerView categoriesList) {
		categoriesList.setLayoutManager(new LinearLayoutManager(context));
		final CategoriesAdapter categoriesAdapter = new CategoriesAdapter(context, getInteractor().getLocalCategories(), true,
			(category, isChecked) -> getInteractor().setCategoryChecked(category, isChecked));
		categoriesList.setAdapter(categoriesAdapter);
		categoriesList.setHasFixedSize(true);
		categoriesList.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
	}

	@Override
	@SuppressWarnings("MissingPermission")
	public void checkCurrentLocation(Context context) {

		// Check location is enabled
		if (!LocationProvider.isLocationEnabled(context)) {
			getView().onLocationIsNotEnabled();
			return;
		}

		// Start location tracker... currently we're not listening to updates.. just grabbing once
		final LocationRequest locationRequest = LocationHelper.getLocationRequest(UPDATE_INTERVAL, FASTEST_UPDATE_INTERVAL);
		locationTrackerSubscription = locationProvider.getUpdatedLocation(locationRequest)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(location -> { // onNext
					getView().onCurrentLocationAcquired(new LatLng(location.getLatitude(), location.getLongitude()));
					locationTrackerSubscription.unsubscribe();
				},
				throwable -> Timber.d("onError: " + throwable.getMessage()), // onError
				() -> locationTrackerSubscription.unsubscribe());// onCompleted
	}

	@Override
	public void getCategories(Context context) {
		// Check if we have local categories, if not load from server
		final boolean isLocalCategoriesEmpty = getInteractor().getLocalCategories().size() == 0;
		final Observable<List<Category>> categoriesObservable = isLocalCategoriesEmpty ?
			getInteractor().getCategories() : getInteractor().getSavedCategories();

		// Subscribe to relevant observable
		categoriesObservable.subscribe(
			categories -> getView().onCategoriesLoaded(categories), // onNext
			throwable -> Timber.d("onError: " + throwable.getMessage()), // onError
			() -> getView().showLoading(false)// onCompleted
		);
	}

	@Override
	@SuppressWarnings("MissingPermission")
	public void getSpots() {
		getSpots(locationProvider.getLastKnownLocation());
	}

	@Override
	@SuppressWarnings("MissingPermission")
	public void getSpots(Observable<Location> locationObservable) {
		if (SHOULD_LOAD_FAKE_DATA) {
			compositeSubscription.add(locationObservable
				.subscribeOn(Schedulers.computation())
				.map(location -> new LatLng(location.getLatitude(), location.getLongitude()))
				.doOnNext(latLng -> getInteractor().saveSpotRequestLocation(latLng))
				.flatMap(latLng -> MapUtils.getRandomSpots(latLng, TEST_SPOTS_COUNT, SPOTS_CALL_RADIUS))
				.onBackpressureBuffer()
				.doOnNext(spots -> spotList.clear())
				.doOnNext(spotList::addAll)
				.observeOn(AndroidSchedulers.mainThread())
				.doOnNext(spots -> getView().clearSpots())
				.doOnNext((spotList) -> getView().renderSpots(spotList, isAtMinZoomLevelToRenderThumbs))
				.subscribe());
			return;
		}

		// TODO: Get User, Get Radius Settings etc.. zip and pass to param builder
		final Observable<List<String>> categoryNamesObservable = getInteractor().getEnabledCategoryNames(categoryHelper);
		compositeSubscription.add(Observable.zip(categoryNamesObservable, locationObservable, this::getSpotApiCallParams)
			.doOnNext(this::saveApiParamsLatLng)
			.flatMap(apiCallParams -> getInteractor().getSpots(apiCallParams))
			.map(SpotsResponse::getData)
			.doOnNext(spots -> spotList.clear())
			.doOnNext(spotList::addAll)
			.doOnNext(spots -> getView().clearSpots())
			.subscribe(spots -> getView().renderSpots(spots, isAtMinZoomLevelToRenderThumbs), // onNext
				throwable -> Timber.d("onError: " + throwable.getMessage()), // onError
				() -> getView().showLoading(false)));// onCompleted
	}

	private void saveApiParamsLatLng(Map<String, String> params) {
		final LatLng latLng = new LatLng(
			Double.valueOf(params.get("lat")),
			Double.valueOf(params.get("lon")));
		getInteractor().saveSpotRequestLocation(latLng);
	}

	@Override
	public void setMapInteractionListeners(GoogleMap googleMap) {
		googleMap.setOnCameraMoveStartedListener(this);
		googleMap.setOnCameraMoveListener(this);
		googleMap.setOnCameraMoveCanceledListener(this);
		googleMap.setOnMyLocationButtonClickListener(this);
	}

	// TODO: Pass in additional params
	private Map<String, String> getSpotApiCallParams(List<String> categoryNames, Location location) {
		final Map<String, String> params = new HashMap<>();
		params.put("username", "Marcus"); // TODO: Param names as constants!
		params.put("categories", Strings.joinOn(",").join(categoryNames));
		params.put("range", String.valueOf(SPOTS_CALL_RADIUS));
		params.put("lat", String.valueOf(location.getLatitude())); // 48.202965
		params.put("lon", String.valueOf(location.getLongitude())); // 16.369017
		return params;
	}

	@Override
	public void showSpotInfoScreen(String id) {
		// TODO: Load spot info screen!
		Timber.d("ID: " + id);
	}

	@Override
	public void onCameraPositionChanged(CameraPosition cameraPosition) {
		if (getInteractor().getSpotRequestLocation().isPresent()) {
			getView().onCameraPositionChanged(cameraPosition);
			final LatLng currentPosition = cameraPosition.target;
			final LatLng lastRequestPosition = getInteractor().getSpotRequestLocation().get();
			final double distanceBetween = computeDistanceBetween(currentPosition, lastRequestPosition);
			final boolean isOutsideOfRadius = distanceBetween > AppConstants.Map.SPOTS_CALL_RADIUS;
			final boolean isAtMinZoomLevelToLoadSpots = cameraPosition.zoom > MIN_ZOOM_LEVEL_TO_LOAD_SPOTS;
			isAtMinZoomLevelToRenderThumbs = cameraPosition.zoom > MIN_ZOOM_LEVEL_TO_RENDER_MARKER_THUMB;
			if (isOutsideOfRadius && isAtMinZoomLevelToLoadSpots) {
				Timber.d(currentPosition.toString());
				compositeSubscription.clear(); // Clear all previous requests
				getView().onMapMovedPastPreviousRadius(currentPosition);
			}
		}
	}

	@Override
	public void checkLocationIsEnabled(Activity activity) {
		final LocationRequest locationRequest = LocationHelper.getLocationRequest(UPDATE_INTERVAL, FASTEST_UPDATE_INTERVAL);
		final LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest).setAlwaysShow(true).build();
		locationProvider.checkLocationSettings(locationSettingsRequest)
			.doOnNext(locationSettingsResult -> {
				final Status status = locationSettingsResult.getStatus();
				if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
					try {
						status.startResolutionForResult(activity, REQUEST_CHECK_LOCATION_SETTINGS);
					} catch (IntentSender.SendIntentException th) {
						Timber.e("Error opening settings activity.", th);
					}
				}
			}).subscribe();
	}

	@Override
	public void onCameraMoveStarted(int reason) {
		switch (reason) {
			case GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE:
				//Timber.d("REASON_GESTURE");
				isMapMoveReasonGesture = true;
				break;
			case GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION:
				Timber.d("REASON_DEVELOPER_ANIMATION");
				break;
			case GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION:
				Timber.d("REASON_API_ANIMATION");
				break;
		}
	}

	@Override
	public void onCameraMove() {
		if (isMapMoveReasonGesture) {
			// TODO: We no longer need to re-render on each camera move
			//getView().renderSpots(spotList, isAtMinZoomLevelToRenderThumbs);
			isMapMoveReasonGesture = false;
		}
	}

	@Override
	public void onCameraMoveCanceled() {
		Timber.d("onCameraMoveCanceled");
	}

	@Override
	public void onCameraIdle() {
		getView().onMapMovedByGesture();
	}

	@Override
	public boolean onMyLocationButtonClick() {
		getView().onLocationButtonClicked();
		return true;
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		// TODO: Implement this for the cluster
		// getView().onSpotInfoWindowClicked(marker);
	}

}
