package io.spotter.ui.map;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.MarkerManager;
import com.google.maps.android.clustering.ClusterManager;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import io.spotter.api.model.Spot;
import io.spotter.ui.listeners.SimpleGoogleMapCallback;
import io.spotter.ui.map.clustering.SpotClusterItem;
import io.spotter.ui.map.clustering.SpotRenderer;
import io.spotter.ui.map.markers.SpotMarker;
import io.spotter.ui.map.markers.SpotMarkerFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static com.google.android.gms.maps.CameraUpdateFactory.newCameraPosition;
import static io.spotter.AppConstants.Map.INITIAL_ZOOM_LEVEL;

public class MapRenderer implements MapContract.View.Renderer {

	private final GoogleMap googleMap;

	private CompositeSubscription compositeSubscription = new CompositeSubscription();

	private final WeakHashMap<String, Marker> visibleMarkers = new WeakHashMap<>();

	private SpotMarkerFactory spotMarkerFactory = new SpotMarkerFactory();

	private final ClusterManager<SpotClusterItem> clusterManager;

	public static MapRenderer getInstance(Context context, GoogleMap googleMap) {

		// Setup cluster manager
		final MarkerManager markerManager = new MarkerManager(googleMap);
		final ClusterManager<SpotClusterItem> clusterManager = new ClusterManager<>(context, googleMap, markerManager);
		final SpotRenderer spotRenderer = new SpotRenderer(context, googleMap, clusterManager);
		clusterManager.setRenderer(spotRenderer);
		clusterManager.setAnimation(false);

		// Set cluster manager listeners
		googleMap.setOnCameraIdleListener(clusterManager);
		googleMap.setOnMarkerClickListener(clusterManager);
		googleMap.setOnInfoWindowClickListener(clusterManager);

		// New map renderer
		return new MapRenderer(googleMap, clusterManager);
	}

	private MapRenderer(GoogleMap googleMap, ClusterManager<SpotClusterItem> clusterManager) {
		this.googleMap = googleMap;
		this.clusterManager = clusterManager;
	}

	@Override
	public void clearSpots(Activity activity) {
		// Clear all markers
		activity.runOnUiThread(() -> {
			visibleMarkers.clear();
			compositeSubscription.clear();
			googleMap.clear();
			clusterManager.clearItems();
		});
	}

	@Override
	@Deprecated
	public void addSpotToMap(SpotMarker spotMarker) {
		final LatLngBounds latLngBounds = googleMap.getProjection().getVisibleRegion().latLngBounds;
		boolean markerIsInBounds = latLngBounds.contains(spotMarker.getMarkerOptions().getPosition());
		boolean markerHasBeenAdded = visibleMarkers.containsKey(spotMarker.getSpot().getId());

		// Add marker if in visible region
		if (markerIsInBounds && !markerHasBeenAdded) {
			final Marker marker = googleMap.addMarker(spotMarker.getMarkerOptions());
			visibleMarkers.put(spotMarker.getSpot().getId(), marker);
		}

		// Remove marker if not in visible region
		if (!markerIsInBounds && markerHasBeenAdded) {
			visibleMarkers.get(spotMarker.getSpot().getId()).remove();
			visibleMarkers.remove(spotMarker.getSpot().getId());
		}
	}

	@Override
	public void renderSpots(List<Spot> spotList, boolean isAtMinZoomLevel) {

		// Add items to cluster
		compositeSubscription.add(Observable.just(spotList)
			.flatMapIterable(spots -> spots)
			.subscribeOn(Schedulers.computation())
			.map(SpotClusterItem::new)
			.toList()
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(clusterManager::addItems,
				throwable -> Timber.d(throwable.getMessage()),
				clusterManager::cluster));

		// Add markers directly to map
		//		compositeSubscription.clear();
		//		compositeSubscription.add(Observable.just(spotList)
		//			.flatMapIterable(spots -> spots)
		//			.subscribeOn(Schedulers.computation())
		//			.onBackpressureBuffer()
		//			.map((model) -> spotMarkerFactory.getMarker(context, model, MarkerType.SIMPLE_MARKER))
		//			//.delay((item) -> Observable.timer(ADD_MARKER_INTERVAL_DELAY, MILLISECONDS))
		//			.observeOn(AndroidSchedulers.mainThread())
		//			.subscribe(this::addSpotToMap, // onNext
		//				throwable -> Timber.d(throwable.getMessage()), // onError
		//				clusterManager::cluster)); // onCompleted
	}

	@Override
	public void onMapMovedByGesture() {
		// TODO: Check zoom level?
		//MarkerManager.Collection markerCollection = clusterManager.getMarkerCollection();
		//Collection<Marker> markers = markerCollection.getMarkers();

		// TODO: Remove cluster items, only add ones in visible window
	}

	@Override
	public void onCurrentLocationAcquired(LatLng currentLatLng, SimpleGoogleMapCallback simpleGoogleMapCallback) {
		final CameraUpdate cameraUpdate = newCameraPosition(new CameraPosition.Builder()
			.target(currentLatLng).zoom(INITIAL_ZOOM_LEVEL).build());
		googleMap.animateCamera(cameraUpdate, simpleGoogleMapCallback);
	}

	@Override
	public void centerMapToLocation(LatLng latLng, SimpleGoogleMapCallback simpleGoogleMapCallback) {
		compositeSubscription.clear();
		clusterManager.clearItems();
		final CameraUpdate cameraUpdate = newCameraPosition(new CameraPosition.Builder()
			.target(latLng).zoom(INITIAL_ZOOM_LEVEL).build());
		googleMap.animateCamera(cameraUpdate, simpleGoogleMapCallback);
	}

	@Override
	public CameraPosition getCameraPosition() {
		return googleMap.getCameraPosition();
	}

	@Override
	public void clearSubscriptions() {
		compositeSubscription.clear();
	}

	@Override
	public Observable<String> getMarkerId(Marker marker) { // TODO: Might be broken
		return Observable.from(visibleMarkers.entrySet())
			.filter(stringMarkerEntry -> stringMarkerEntry.getValue().equals(marker))
			.map(Map.Entry::getKey);
	}

	/**
	 * TODO: Sort markers, closest first, should speed up first load
	 */
	public class SortPlaces implements Comparator<Spot> {
		LatLng currentLoc;

		public SortPlaces(LatLng current) {
			currentLoc = current;
		}

		@Override
		public int compare(final Spot spot1, final Spot spot2) {

			double lat1 = spot1.getLocatedAt().getLocation().getLatitude();
			double lon1 = spot1.getLocatedAt().getLocation().getLongitude();

			double lat2 = spot2.getLocatedAt().getLocation().getLatitude();
			double lon2 = spot2.getLocatedAt().getLocation().getLongitude();

			double distanceToPlace1 = distance(currentLoc.latitude, currentLoc.longitude, lat1, lon1);
			double distanceToPlace2 = distance(currentLoc.latitude, currentLoc.longitude, lat2, lon2);
			return (int) (distanceToPlace1 - distanceToPlace2);
		}

		public double distance(double fromLat, double fromLon, double toLat, double toLon) {
			double radius = 6378137;   // approximate Earth radius, *in meters*
			double deltaLat = toLat - fromLat;
			double deltaLon = toLon - fromLon;
			double angle = 2 * Math.asin(Math.sqrt(
				Math.pow(Math.sin(deltaLat / 2), 2) +
					Math.cos(fromLat) * Math.cos(toLat) *
						Math.pow(Math.sin(deltaLon / 2), 2)));
			return radius * angle;
		}
	}
}
