package io.spotter.ui.map.clustering;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import io.spotter.api.model.Spot;

public class SpotClusterItem implements ClusterItem {

	private final Spot spot;

	private final LatLng spotPosition;

	public SpotClusterItem(Spot spot) {
		this.spot = spot;
		this.spotPosition = new LatLng(
			spot.getLocatedAt().getLocation().getLatitude(),
			spot.getLocatedAt().getLocation().getLongitude());
	}

	@Override
	public LatLng getPosition() {
		return spotPosition;
	}

	@Override
	public String getTitle() {
		return spot.getOwner().getUsername();
	}

	@Override
	public String getSnippet() {
		return spot.getDescription();
	}

	public Spot getSpot() {
		return spot;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpotClusterItem that = (SpotClusterItem) o;

		if (spot != null ? !spot.equals(that.spot) : that.spot != null) return false;
		return spotPosition != null ? spotPosition.equals(that.spotPosition) : that.spotPosition == null;

	}

	@Override
	public int hashCode() {
		int result = spot != null ? spot.hashCode() : 0;
		result = 31 * result + (spotPosition != null ? spotPosition.hashCode() : 0);
		return result;
	}

}
