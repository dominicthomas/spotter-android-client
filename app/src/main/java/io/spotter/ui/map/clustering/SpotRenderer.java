package io.spotter.ui.map.clustering;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.util.HashMap;
import java.util.Map;

import io.spotter.R;
import io.spotter.api.model.Spot;
import io.spotter.utils.CategoryHelper;
import io.spotter.utils.ImageLoader;
import timber.log.Timber;

public class SpotRenderer extends DefaultClusterRenderer<SpotClusterItem> {

	private final Context context;

	private final IconGenerator iconGenerator;

	private final ImageView markerImageView;

	private final int dimensions;

	public SpotRenderer(Context context, GoogleMap map, ClusterManager<SpotClusterItem> clusterManager) {
		super(context, map, clusterManager);
		this.context = context;
		iconGenerator = new IconGenerator(context);
		markerImageView = new ImageView(context);
		dimensions = (int) context.getResources().getDimension(R.dimen.custom_marker_image_width);
		markerImageView.setLayoutParams(new ViewGroup.LayoutParams(dimensions, dimensions));
		iconGenerator.setContentView(markerImageView);
	}

	@Override
	protected void onBeforeClusterItemRendered(SpotClusterItem clusterItem, MarkerOptions markerOptions) {
		super.onBeforeClusterItemRendered(clusterItem, markerOptions);

		// Set default marker
		markerOptions.position(clusterItem.getPosition()).icon(
			CategoryHelper.getIcon(clusterItem.getSpot().getCategory()));
	}

	@Override
	protected void onClusterItemRendered(SpotClusterItem clusterItem, Marker marker) {
		super.onClusterItemRendered(clusterItem, marker);

		// Check for spot
		final Spot spot = clusterItem.getSpot();
		if (spot == null) return;

		// Load image from url
		final ImageViewIconTarget imageViewIconTarget = new ImageViewIconTarget(spot, marker, iconGenerator, markerImageView);
		ImageLoader.loadImageFromUrl(context, clusterItem.getSpot().getImageUrl(), dimensions, dimensions, imageViewIconTarget);
	}

	// TODO: Customise cluster icons

	@Override
	protected boolean shouldRenderAsCluster(Cluster<SpotClusterItem> cluster) {
		return cluster.getSize() > 2; // Overlapping markers before clustering
	}

	/**
	 * This is a simple imageview target but for custom rendering.. subclass SimpleTarget and do
	 * whatever you want in the onResourceReady callback.
	 */
	private class ImageViewIconTarget extends SimpleTarget<GlideDrawable> {

		private final Spot spot;

		private final Marker marker;

		private final ImageView markerImageView;

		private final IconGenerator iconGenerator;

		private ImageViewIconTarget(Spot spot, Marker marker, IconGenerator iconGenerator, ImageView markerImageView) {
			this.spot = spot;
			this.marker = marker;
			this.markerImageView = markerImageView;
			this.iconGenerator = iconGenerator;
		}

		@Override
		public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
			if (getCluster(marker) != null || getClusterItem(marker) != null) { // or check tag
				markerImageView.setImageDrawable(resource);
				iconGenerator.setColor(CategoryHelper.getColor(spot.getCategory()));
				final Bitmap markerBitmap = iconGenerator.makeIcon();
				marker.setIcon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
			}
		}

		@Override
		public void onLoadFailed(Exception e, Drawable errorDrawable) {
			super.onLoadFailed(e, errorDrawable);
			Timber.d("onLoadFailed: " + spot.getImageUrl());
		}
	}
}
