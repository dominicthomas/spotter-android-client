package io.spotter.ui.map.markers;


public abstract class BaseMapMarker<T> implements MarkerRenderer {

	private T model;

	public BaseMapMarker(T model) {
		this.model = model;
	}

	public T getModel() {
		return model;
	}

}
