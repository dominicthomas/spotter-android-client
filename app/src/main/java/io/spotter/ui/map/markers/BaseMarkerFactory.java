package io.spotter.ui.map.markers;

import android.content.Context;

public abstract class BaseMarkerFactory<T> {

	public abstract SpotMarker getMarker(Context context, T model, MarkerType markerType);
}
