package io.spotter.ui.map.markers;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.ColorUtils;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import io.spotter.api.model.Spot;
import io.spotter.utils.CategoryHelper;

public class IconMarker extends BaseMapMarker<Spot> {

	public IconMarker(Spot model) {
		super(model);
	}

	@Override
	public SpotMarker render(Context context) {
		final Double latitude = getModel().getLocatedAt().getLocation().getLatitude();
		final Double longitude = getModel().getLocatedAt().getLocation().getLongitude();
		final LatLng locatedAt = new LatLng(latitude, longitude);

		float colorHueForCategory = CategoryHelper.getColorHueForCategory(getModel().getCategory());
		int hslColor = ColorUtils.HSLToColor(new float[]{colorHueForCategory, 1.0f, 1.0f});

		final IconGenerator iconGenerator = new IconGenerator(context);
		iconGenerator.setColor(hslColor);

		//ColorDrawable colorDrawable = new ColorDrawable(hslColor);
		//ContextCompat.getColor(this, R.color.white)
		//iconGenerator.setBackground(colorDrawable);
		//iconGenerator.setContentView(); // TODO: Create custom view and load image

		// NEW: TODO
		// Load url
		// Set background color
		// Create bitmap with view
		// Add to map
		// Enable clustering!

		Bitmap bitmap = iconGenerator.makeIcon("HELLO!!!!");

		final MarkerOptions markerOptions = new MarkerOptions()
			.position(locatedAt)
			.icon(BitmapDescriptorFactory.fromBitmap(bitmap))
			.title(getModel().getDescription())
			.snippet(getModel().getOwner().getUsername())
			.anchor(0.5f, 1);
		return new SpotMarker(getModel(), markerOptions);
	}
}
