package io.spotter.ui.map.markers;

import android.content.Context;

public interface MarkerRenderer {

	SpotMarker render(Context context);
}
