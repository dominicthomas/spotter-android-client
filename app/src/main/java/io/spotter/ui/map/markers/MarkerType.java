package io.spotter.ui.map.markers;

public enum MarkerType {
	SIMPLE_MARKER, MARKER_ICON
}