package io.spotter.ui.map.markers;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import io.spotter.api.model.Spot;
import io.spotter.utils.CategoryHelper;

public class SimpleMarker extends BaseMapMarker<Spot> {

	public SimpleMarker(Spot model) {
		super(model);
	}

	@Override
	public SpotMarker render(Context context) {
		final Double latitude = getModel().getLocatedAt().getLocation().getLatitude();
		final Double longitude = getModel().getLocatedAt().getLocation().getLongitude();
		final LatLng locatedAt = new LatLng(latitude, longitude);
		final MarkerOptions markerOptions = new MarkerOptions()
			.position(locatedAt)
			.icon(CategoryHelper.getIcon(getModel().getCategory()))
			.title(getModel().getDescription())
			.snippet(getModel().getOwner().getUsername())
			.anchor(0.5f, 1);
		return new SpotMarker(getModel(), markerOptions);
	}

}
