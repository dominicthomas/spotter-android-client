package io.spotter.ui.map.markers;



import com.google.android.gms.maps.model.MarkerOptions;

import io.spotter.api.model.Spot;

public class SpotMarker {

	private final Spot spot;

	private final MarkerOptions marker;

	public SpotMarker(Spot spot, MarkerOptions marker) {
		this.spot = spot;
		this.marker = marker;
	}

	public Spot getSpot() {
		return spot;
	}

	public MarkerOptions getMarkerOptions() {
		return marker;
	}
}
