package io.spotter.ui.map.markers;

import android.content.Context;

import io.spotter.api.model.Spot;

@Deprecated
public class SpotMarkerFactory extends BaseMarkerFactory<Spot> {

	@Override
	public SpotMarker getMarker(Context context, Spot model, MarkerType markerType) {
		switch (markerType) {
			case SIMPLE_MARKER:
				return new SimpleMarker(model).render(context);
			case MARKER_ICON:
				return new IconMarker(model).render(context);
			default:
				return null;
		}
	}
}
