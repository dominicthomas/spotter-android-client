package io.spotter.ui.notifications;

import io.spotter.R;
import io.spotter.ui.base.BaseMVPFragment;
import io.spotter.app.mvp.FragmentPresenter;

public class NotificationsFragment extends BaseMVPFragment {

	@Override
	public int getLayoutId() {
		return R.layout.fragment_notifications;
	}

	@Override
	protected FragmentPresenter initPresenter() {
		return null;
	}

}
