package io.spotter.ui.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;

public class ColorSwitch extends SwitchCompat {

	private final static int UNCHECKED_ALPHA = 60;

	private int primaryColor = -1;

	public ColorSwitch(Context context) {
		super(context);
	}

	public ColorSwitch(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ColorSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void setPrimaryColor(int primaryColor) {
		this.primaryColor = primaryColor;
	}

	@Override
	public void setChecked(boolean checked) {
		super.setChecked(checked);

		// Set on/off color..
		if (primaryColor == 0) return;
		int thumbColor = isChecked() ? primaryColor : ColorUtils.setAlphaComponent(primaryColor, UNCHECKED_ALPHA);
		getThumbDrawable().setColorFilter(thumbColor, PorterDuff.Mode.SRC_ATOP );
	}

}
