package io.spotter.utils;

import android.graphics.Color;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.ArrayList;
import java.util.List;

import io.spotter.api.model.Category;
import io.spotter.ui.categories.CategoryColor;

import static com.google.android.gms.maps.model.BitmapDescriptorFactory.defaultMarker;

public class CategoryHelper {

	private final List<String> categoryNames = new ArrayList<>();

	private final List<String> categoryIds = new ArrayList<>();

	public List<String> getEnabledCategoryNameList(List<Category> categories) {
		int size = categories.size();
		for (int i = 0; i < size; i++) {
			final Category category = categories.get(i);
			if (!category.isEnabled()) continue;
			categoryNames.add(category.getName());
			if (!category.getChildren().isEmpty()) {
				getEnabledCategoryNameList(category.getChildren());
			}
		}
		return categoryNames;
	}

	public List<String> getEnabledCategoryIdList(List<Category> categories) {
		int size = categories.size();
		for (int i = 0; i < size; i++) {
			final Category category = categories.get(i);
			if (!category.isEnabled()) continue;
			categoryIds.add(category.getId());
			if (!category.getChildren().isEmpty()) {
				getEnabledCategoryNameList(category.getChildren());
			}
		}
		return categoryIds;
	}

	public static int getColor(Category category) {
		float[] hsv = new float[]{getColorHueForCategory(category), 1.0f, 1.0f};
		return Color.HSVToColor(hsv);
	}

	public static float getColorHueForCategory(Category category) {
		final String categoryId = category.getDepth() == 0 ? category.getId() : category.getParentId();
		return getColorHueForId(categoryId);
	}

	private static float getColorHueForId(String categoryId) {
		switch (categoryId) {
			case "4d8c9c9c-762a-41dd-9f17-9f1706563df9":
				return CategoryColor.CULTURAL.getColor();
			case "da546976-f826-4982-ba63-d9281c491a80":
				return CategoryColor.ENTERTAINMENT.getColor();
			case "69e00a4d-13fa-4e6e-abc1-b4d18f24e728":
				return CategoryColor.EVENTS.getColor();
			case "e6ad1043-f05e-44dd-8aeb-eb13c37400e6":
				return CategoryColor.SHARING.getColor();
			case "81405038-dd60-4675-a72c-b8456a3a1ac4":
				return CategoryColor.SHOPPING.getColor();
			case "6afbbe10-e4ec-446a-ab18-ea21ca2628a4":
				return CategoryColor.SPORTS.getColor();
			default:
				return 0f;
		}
	}

	public static BitmapDescriptor getIcon(Category category) {
		float hue = getColorHueForCategory(category);
		return defaultMarker(hue);
	}

	public static BitmapDescriptor getMarkerIcon(String color) {
		float[] hsv = new float[3];
		Color.colorToHSV(Color.parseColor(color), hsv);
		return BitmapDescriptorFactory.defaultMarker(hsv[0]);
	}

}