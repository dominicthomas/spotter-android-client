package io.spotter.utils;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.SimpleTarget;

public class ImageLoader {

	public static void loadImageFromUrl(Context context, String imageUrl, int width, int height, SimpleTarget<GlideDrawable> simpleTarget) {
		if (context == null) return;

		// Load image from model url
		Glide.with(context.getApplicationContext())
			.load(imageUrl)
			.diskCacheStrategy(DiskCacheStrategy.RESULT)
			.override(width, height)
			.dontAnimate()
			.into(simpleTarget);
	}

}
