package io.spotter.utils;

import android.app.Activity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.LocationRequest;

public class LocationHelper {

	public static final int GOOGLE_PLAY_SERVICES_REQUEST = 200;

	public static final int UPDATE_INTERVAL = 15000;

	public static final int FASTEST_UPDATE_INTERVAL = 5000;

	public static final int EXPIRATION_DURATION = 60000;

	// See: https://developers.google.com/android/reference/com/google/android/gms/location/LocationRequest
	public static LocationRequest getLocationRequest(int interval, int fastestInterval) {
		return LocationRequest.create()
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
			.setInterval(interval)
			.setFastestInterval(fastestInterval);
	}

	public static boolean isGooglePlayServicesAvailable(Activity activity, int requestCode) {
		GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
		int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
		if (status != ConnectionResult.SUCCESS) {
			if (googleApiAvailability.isUserResolvableError(status)) {
				googleApiAvailability.getErrorDialog(activity, status, requestCode).show();
			}
			return false;
		}
		return true;
	}

}
