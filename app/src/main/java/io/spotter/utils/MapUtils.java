package io.spotter.utils;

import android.graphics.Color;

import com.fernandocejas.arrow.collections.Lists;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;
import java.util.Map;
import java.util.Random;

import io.spotter.api.model.Category;
import io.spotter.api.model.LocatedAt;
import io.spotter.api.model.Location;
import io.spotter.api.model.Owner;
import io.spotter.api.model.Spot;
import rx.Observable;
import rx.Subscriber;

import static com.google.android.gms.maps.model.BitmapDescriptorFactory.defaultMarker;

public class MapUtils {

	private static final List<Float> MARKER_COLOURS = Lists.newArrayList(
		BitmapDescriptorFactory.HUE_AZURE,
		BitmapDescriptorFactory.HUE_BLUE,
		BitmapDescriptorFactory.HUE_CYAN,
		BitmapDescriptorFactory.HUE_GREEN,
		BitmapDescriptorFactory.HUE_MAGENTA,
		BitmapDescriptorFactory.HUE_ORANGE,
		BitmapDescriptorFactory.HUE_RED,
		BitmapDescriptorFactory.HUE_ROSE,
		BitmapDescriptorFactory.HUE_VIOLET,
		BitmapDescriptorFactory.HUE_YELLOW);

	private static final List<String> CATEGORY_IDS = Lists.newArrayList(
		"4d8c9c9c-762a-41dd-9f17-9f1706563df9",
		"da546976-f826-4982-ba63-d9281c491a80",
		"69e00a4d-13fa-4e6e-abc1-b4d18f24e728",
		"e6ad1043-f05e-44dd-8aeb-eb13c37400e6",
		"81405038-dd60-4675-a72c-b8456a3a1ac4",
		"6afbbe10-e4ec-446a-ab18-ea21ca2628a4");

	public static Observable<LatLng> randomLatLngObservable(LatLng currentLocation, int numberOfSpots, int radius) {
		return Observable.create(new Observable.OnSubscribe<LatLng>() {
			@Override
			public void call(Subscriber<? super LatLng> subscriber) {
				try {
					for (int i = 0; i < numberOfSpots; i++) {
						double latitude = currentLocation.latitude;
						double longitude = currentLocation.longitude;
						LatLng latLng = generateRandomLocation(latitude, longitude, radius);
						subscriber.onNext(latLng); // Output random location
					}
					subscriber.onCompleted(); // Finished
				} catch (Exception e) {
					subscriber.onError(e);
				}
			}
		});
	}

//	public static Observable<SpotMarker> randomMarkerOptionObservable(LatLng currentLocation, int numberOfSpots, int radius) {
//		return Observable.create(subscriber -> { // TODO: Implement range
//			try {
//				for (int i = 0; i < numberOfSpots; i++) {
//					double latitude = currentLocation.latitude;
//					double longitude = currentLocation.longitude;
//					final LatLng latLng = generateRandomLocation(latitude, longitude, radius);
//
//					final MarkerOptions markerOptions = new MarkerOptions().position(latLng)
//						.anchor(0.5f, 1)
//						.icon(BitmapDescriptorFactory.defaultMarker(getRandomMarkerColor()));
//
//					subscriber.onNext(new SpotMarker(new Spot(), markerOptions)); // Output marker
//				}
//				subscriber.onCompleted(); // Finished
//			} catch (Exception e) {
//				subscriber.onError(e);
//			}
//		});
//	}

	public static Observable<List<Spot>> getRandomSpots(LatLng currentLocation, int count, int radius) {
		return Observable.range(0, count).map(integer -> {
			double latitude = currentLocation.latitude;
			double longitude = currentLocation.longitude;
			final LatLng latLng = generateRandomLocation(latitude, longitude, radius);

			final Location location = new Location();
			location.setLatitude(latLng.latitude);
			location.setLongitude(latLng.longitude);

			final LocatedAt locatedAt = new LocatedAt();
			locatedAt.setLocation(location);

			final Category category = new Category();
			category.setId(getRandomCategoryId());

			final Owner owner = new Owner();
			owner.setUsername("some_username");

			final Spot spot = new Spot();
			spot.setLocatedAt(locatedAt);
			spot.setId(String.valueOf(integer));
			spot.setCategory(category);
			spot.setDescription("Some Title");
			spot.setImageUrl("https://unsplash.it/100/100?image=" + String.valueOf(integer));
			spot.setOwner(owner);

			return spot;
		}).toList();
	}

	public static String getRandomCategoryId() {
		Random random = new Random();
		return CATEGORY_IDS.get(random.nextInt(CATEGORY_IDS.size()));
	}

	public static Float getRandomMarkerColor() {
		Random random = new Random();
		return MARKER_COLOURS.get(random.nextInt(MARKER_COLOURS.size()));
	}

	public static LatLng generateRandomLocation(double latitude, double longitude, int radiusMeters) {
		Random random = new Random();

		// Convert radius from meters to degrees
		double radiusInDegrees = radiusMeters / 111000f;

		double u = random.nextDouble();
		double v = random.nextDouble();
		double w = radiusInDegrees * Math.sqrt(u);
		double t = 2 * Math.PI * v;
		double x = w * Math.cos(t);
		double y = w * Math.sin(t);

		// Adjust the x-coordinate for the shrinking of the east-west distances
		double new_x = x / Math.cos(Math.toRadians(latitude));

		double foundLongitude = new_x + longitude;
		double foundLatitude = y + latitude;

		return new LatLng(foundLatitude, foundLongitude);
	}

	public BitmapDescriptor getMarkerIcon(String colorHex) {
		float[] hsv = new float[3];
		Color.colorToHSV(Color.parseColor(colorHex), hsv);
		return defaultMarker(hsv[0]);
	}

	public static void removeMarkersFromMap(Map<String, Marker> visibleMarkers) {
		for (Map.Entry<String, Marker> entry : visibleMarkers.entrySet()) {
			entry.getValue().remove();
			visibleMarkers.remove(entry.getKey());
		}
	}

	public static Observable<android.location.Location> createLocationObservable(LatLng currentPosition) {
		return Observable.just(new android.location.Location(""))
			.doOnNext(location -> location.setLatitude(currentPosition.latitude))
			.doOnNext(location -> location.setLongitude(currentPosition.longitude));
	}
}
