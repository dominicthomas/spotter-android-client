package io.spotter.utils;


import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ActivityCompat;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class PermissionsHelper {

	public static final int LOCATION_PERMISSION_REQUEST = 100;

	public static boolean hasLocationPermissions(Context context) {
		return ActivityCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED &&
			ActivityCompat.checkSelfPermission(context, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED;
	}

	public static void requestLocationPermissions(Activity activity, int requestCode) {
		ActivityCompat.requestPermissions(activity,
			new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, requestCode);
	}

}
